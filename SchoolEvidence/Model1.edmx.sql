
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 06/07/2022 22:44:26
-- Generated from EDMX file: C:\Users\XY\source\repos\school-evidence\SchoolEvidence\Model1.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [Database];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_StudentClassStudent]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[StudentClassSet] DROP CONSTRAINT [FK_StudentClassStudent];
GO
IF OBJECT_ID(N'[dbo].[FK_StudentClassExcuse]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[StudentClassSet] DROP CONSTRAINT [FK_StudentClassExcuse];
GO
IF OBJECT_ID(N'[dbo].[FK_StudentExcuse]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[ExcuseSet] DROP CONSTRAINT [FK_StudentExcuse];
GO
IF OBJECT_ID(N'[dbo].[FK_GroupClass]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[ClassSet] DROP CONSTRAINT [FK_GroupClass];
GO
IF OBJECT_ID(N'[dbo].[FK_GroupStudentStudent]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GroupStudentSet] DROP CONSTRAINT [FK_GroupStudentStudent];
GO
IF OBJECT_ID(N'[dbo].[FK_GroupStudentGroup]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GroupStudentSet] DROP CONSTRAINT [FK_GroupStudentGroup];
GO
IF OBJECT_ID(N'[dbo].[FK_ClassStudentClass]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[StudentClassSet] DROP CONSTRAINT [FK_ClassStudentClass];
GO
IF OBJECT_ID(N'[dbo].[FK_SubjectGroup]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[GroupSet] DROP CONSTRAINT [FK_SubjectGroup];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[ClassSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ClassSet];
GO
IF OBJECT_ID(N'[dbo].[StudentSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[StudentSet];
GO
IF OBJECT_ID(N'[dbo].[ExcuseSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ExcuseSet];
GO
IF OBJECT_ID(N'[dbo].[StudentClassSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[StudentClassSet];
GO
IF OBJECT_ID(N'[dbo].[GroupSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GroupSet];
GO
IF OBJECT_ID(N'[dbo].[GroupStudentSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GroupStudentSet];
GO
IF OBJECT_ID(N'[dbo].[SubjectSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[SubjectSet];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'ClassSet'
CREATE TABLE [dbo].[ClassSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [DateTime] datetime  NOT NULL,
    [GroupId] int  NOT NULL
);
GO

-- Creating table 'StudentSet'
CREATE TABLE [dbo].[StudentSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [Surname] nvarchar(max)  NOT NULL,
    [DateOfBirth] datetime  NOT NULL,
    [Gender] int  NOT NULL,
    [Address] nvarchar(max)  NOT NULL,
    [Email] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'ExcuseSet'
CREATE TABLE [dbo].[ExcuseSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [StudentId] int  NOT NULL,
    [DateFrom] datetime  NOT NULL,
    [DateTo] datetime  NOT NULL,
    [Reason] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'StudentClassSet'
CREATE TABLE [dbo].[StudentClassSet] (
    [StudentId] int  NOT NULL,
    [ClassId] int  NOT NULL,
    [ExcuseId] int  NULL,
    [Attended] bit  NOT NULL
);
GO

-- Creating table 'GroupSet'
CREATE TABLE [dbo].[GroupSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [Day] int  NOT NULL,
    [Time] nvarchar(max)  NOT NULL,
    [SubjectId] int  NOT NULL
);
GO

-- Creating table 'GroupStudentSet'
CREATE TABLE [dbo].[GroupStudentSet] (
    [StudentId] int  NOT NULL,
    [GroupId] int  NOT NULL
);
GO

-- Creating table 'SubjectSet'
CREATE TABLE [dbo].[SubjectSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Code] nvarchar(max)  NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [Year] int  NOT NULL,
    [Term] int  NOT NULL,
    [Credit] int  NOT NULL,
    [DurationHours] int  NOT NULL,
    [Completion] int  NOT NULL,
    [Prerequisites] nvarchar(max)  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id] in table 'ClassSet'
ALTER TABLE [dbo].[ClassSet]
ADD CONSTRAINT [PK_ClassSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'StudentSet'
ALTER TABLE [dbo].[StudentSet]
ADD CONSTRAINT [PK_StudentSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'ExcuseSet'
ALTER TABLE [dbo].[ExcuseSet]
ADD CONSTRAINT [PK_ExcuseSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [StudentId], [ClassId] in table 'StudentClassSet'
ALTER TABLE [dbo].[StudentClassSet]
ADD CONSTRAINT [PK_StudentClassSet]
    PRIMARY KEY CLUSTERED ([StudentId], [ClassId] ASC);
GO

-- Creating primary key on [Id] in table 'GroupSet'
ALTER TABLE [dbo].[GroupSet]
ADD CONSTRAINT [PK_GroupSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [StudentId], [GroupId] in table 'GroupStudentSet'
ALTER TABLE [dbo].[GroupStudentSet]
ADD CONSTRAINT [PK_GroupStudentSet]
    PRIMARY KEY CLUSTERED ([StudentId], [GroupId] ASC);
GO

-- Creating primary key on [Id] in table 'SubjectSet'
ALTER TABLE [dbo].[SubjectSet]
ADD CONSTRAINT [PK_SubjectSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [StudentId] in table 'StudentClassSet'
ALTER TABLE [dbo].[StudentClassSet]
ADD CONSTRAINT [FK_StudentClassStudent]
    FOREIGN KEY ([StudentId])
    REFERENCES [dbo].[StudentSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [ExcuseId] in table 'StudentClassSet'
ALTER TABLE [dbo].[StudentClassSet]
ADD CONSTRAINT [FK_StudentClassExcuse]
    FOREIGN KEY ([ExcuseId])
    REFERENCES [dbo].[ExcuseSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_StudentClassExcuse'
CREATE INDEX [IX_FK_StudentClassExcuse]
ON [dbo].[StudentClassSet]
    ([ExcuseId]);
GO

-- Creating foreign key on [StudentId] in table 'ExcuseSet'
ALTER TABLE [dbo].[ExcuseSet]
ADD CONSTRAINT [FK_StudentExcuse]
    FOREIGN KEY ([StudentId])
    REFERENCES [dbo].[StudentSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_StudentExcuse'
CREATE INDEX [IX_FK_StudentExcuse]
ON [dbo].[ExcuseSet]
    ([StudentId]);
GO

-- Creating foreign key on [GroupId] in table 'ClassSet'
ALTER TABLE [dbo].[ClassSet]
ADD CONSTRAINT [FK_GroupClass]
    FOREIGN KEY ([GroupId])
    REFERENCES [dbo].[GroupSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GroupClass'
CREATE INDEX [IX_FK_GroupClass]
ON [dbo].[ClassSet]
    ([GroupId]);
GO

-- Creating foreign key on [StudentId] in table 'GroupStudentSet'
ALTER TABLE [dbo].[GroupStudentSet]
ADD CONSTRAINT [FK_GroupStudentStudent]
    FOREIGN KEY ([StudentId])
    REFERENCES [dbo].[StudentSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [GroupId] in table 'GroupStudentSet'
ALTER TABLE [dbo].[GroupStudentSet]
ADD CONSTRAINT [FK_GroupStudentGroup]
    FOREIGN KEY ([GroupId])
    REFERENCES [dbo].[GroupSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GroupStudentGroup'
CREATE INDEX [IX_FK_GroupStudentGroup]
ON [dbo].[GroupStudentSet]
    ([GroupId]);
GO

-- Creating foreign key on [ClassId] in table 'StudentClassSet'
ALTER TABLE [dbo].[StudentClassSet]
ADD CONSTRAINT [FK_ClassStudentClass]
    FOREIGN KEY ([ClassId])
    REFERENCES [dbo].[ClassSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ClassStudentClass'
CREATE INDEX [IX_FK_ClassStudentClass]
ON [dbo].[StudentClassSet]
    ([ClassId]);
GO

-- Creating foreign key on [SubjectId] in table 'GroupSet'
ALTER TABLE [dbo].[GroupSet]
ADD CONSTRAINT [FK_SubjectGroup]
    FOREIGN KEY ([SubjectId])
    REFERENCES [dbo].[SubjectSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_SubjectGroup'
CREATE INDEX [IX_FK_SubjectGroup]
ON [dbo].[GroupSet]
    ([SubjectId]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------