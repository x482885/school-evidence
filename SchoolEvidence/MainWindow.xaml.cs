﻿using SchoolEvidence.DAO;
using SchoolEvidence.Views;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace SchoolEvidence
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public ClassDao Class_Dao { get; private set; } = new ClassDao();
        public ExcuseDao Excuse_Dao { get; private set; } = new ExcuseDao();
        public StudentDao Student_Dao { get; private set; } = new StudentDao();
        public SubjectDao Subject_Dao { get; private set; } = new SubjectDao();
        public GroupDao Group_Dao { get; private set; } = new GroupDao();
        public StudentClassDao StudentClass_Dao { get; private set; } = new StudentClassDao();
        public GroupStudentDao GroupStudent_Dao { get; private set; } = new GroupStudentDao();
        private readonly HomeView Home_View;
        private readonly StudentsView Students_View;
        private readonly SubjectsView Subjects_View;
        public int SpringTermStartMonth { get; private set; } = 3;
        public int AutumnTermStartMonth { get; private set; } = 9;
        public int TermWeeks { get; private set; } = 12;
        private readonly List<ViewInterface> ReturnButtonSequence = new List<ViewInterface>();


        public MainWindow()
        {
            InitializeComponent();
            Home_View = new HomeView(this);
            Students_View = new StudentsView(this);
            Subjects_View = new SubjectsView(this);
            ContentControl.Content = Home_View;
        }

        private void HomeButton_Click(object sender, RoutedEventArgs e)
        {
            Home_View.UpdateUI();
            ReturnButtonSequence.Clear();
            SwitchContext(Home_View);
        }

        private void SubjectsButton_Click(object sender, RoutedEventArgs e)
        {
            Subjects_View.UpdateUI();
            ReturnButtonSequence.Clear();
            SwitchContext(Subjects_View);
        }

        private void StudentButton_Click(object sender, RoutedEventArgs e)
        {
            Students_View.UpdateUI();
            ReturnButtonSequence.Clear();
            SwitchContext(Students_View);
        }

        public void SwitchContext(ViewInterface view)
        {
            ContentControl.Content = view;
            ReturnButtonSequence.Add(view);
            setBackbuttonVisible();
        }

        private void BackButton_Click(object sender, RoutedEventArgs e)
        {
            ContentControl.Content = ReturnButtonSequence[ReturnButtonSequence.Count - 2];
            ReturnButtonSequence.Remove(ReturnButtonSequence.Last());
            setBackbuttonVisible();
        }

        private void setBackbuttonVisible()
        {
            if (ReturnButtonSequence.Count > 1)
            {
                BackButton.Visibility = Visibility.Visible;
                BackButton.IsEnabled = true;
            }
            else
            {
                BackButton.Visibility = Visibility.Hidden;
                BackButton.IsEnabled = false;
            }
        }

    }
}
