﻿using SchoolEvidence.DAO;
using SchoolEvidence.Entities;
using SchoolEvidence.Tools;
using System;
using System.Linq;
using System.Windows;

namespace SchoolEvidence.Dialogs
{
    /// <summary>
    /// Interaction logic for AddSubjectDialog.xaml
    /// </summary>
    public partial class AddSubjectDialog : Window
    {
        private readonly SubjectDao Dao;
        private readonly bool Editing = false;
        private readonly Subject Subject = null;
        public AddSubjectDialog(SubjectDao dao)
        {
            InitializeComponent();
            Dao = dao;
            Title = "Add subject";
            completionComboBox.ItemsSource = Enum.GetValues(typeof(CompletionType));
            yearTextBox.PreviewTextInput += Utils.NumberValidationTextBox;
            creditTextBox.PreviewTextInput += Utils.NumberValidationTextBox;
            durationTextBox.PreviewTextInput += Utils.NumberValidationTextBox;
        }

        public AddSubjectDialog(SubjectDao dao, bool editing, Subject subject) : this(dao)
        {
            Editing = editing;
            Subject = subject;
            addBtn.Content = "Confirm";
        }

        private bool CheckValues()
        {
            bool returnBool = true;
            codeTextBox.Text.Trim();
            nameTextBox.Text.Trim();
            yearTextBox.Text.Trim();
            if (Utils.IsNullOrEmptyOrBlank(codeTextBox.Text)) { codeTextBox.BorderBrush = Utils.warningColor; returnBool = false; }
            if (Utils.IsNullOrEmptyOrBlank(nameTextBox.Text)) { nameTextBox.BorderBrush = Utils.warningColor; returnBool = false; }
            if (Utils.IsNullOrEmptyOrBlank(yearTextBox.Text) || !int.TryParse(yearTextBox.Text, out int resYear)) { yearTextBox.BorderBrush = Utils.warningColor; returnBool = false; }
            if (Utils.IsNullOrEmptyOrBlank(creditTextBox.Text) || !int.TryParse(yearTextBox.Text, out int resCredit)) { creditTextBox.BorderBrush = Utils.warningColor; returnBool = false; }
            if (Utils.IsNullOrEmptyOrBlank(durationTextBox.Text) || !int.TryParse(durationTextBox.Text, out int resDuration)) { durationTextBox.BorderBrush = Utils.warningColor; returnBool = false; }
            if (radioTermSpring.IsChecked == null || radioTermAutumn.IsChecked == null) returnBool = false;
            if (completionComboBox.SelectedIndex == -1) { completionComboBox.BorderBrush = Utils.warningColor; returnBool = false; }
            if (Dao.findAll().Select(s => s.Name).Contains(codeTextBox.Text.Trim()))
            {
                MessageBox.Show("Subject with this code already exists.");
                returnBool = false;
            };
            return returnBool;
        }

        private void cancelBtn_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

        private void addBtn_Click(object sender, RoutedEventArgs e)
        {
            if (!CheckValues()) return;
            if (Editing) { Edit(); return; }
            Dao.insert(new SubjectStruct(
                codeTextBox.Text.Trim(),
                nameTextBox.Text.Trim(),
                int.Parse(yearTextBox.Text.Trim()),
                (bool)radioTermSpring.IsChecked ? Term.Spring : Term.Autumn,
                int.Parse(creditTextBox.Text.Trim()),
                int.Parse(durationTextBox.Text.Trim()),
                (CompletionType)completionComboBox.SelectedIndex,
                prerequisitesTextBox.Text.Trim()
                ));
            DialogResult = true;
        }

        private void Edit()
        {
            codeTextBox.Text = Subject.Code;
            nameTextBox.Text = Subject.Name;
            yearTextBox.Text = Subject.Year.ToString();
            creditTextBox.Text = Subject.Credit.ToString();
            durationTextBox.Text = Subject.DurationHours.ToString();
            prerequisitesTextBox.Text = Subject.Prerequisites;
            radioTermAutumn.IsChecked = Subject.Term == Term.Autumn;
            radioTermSpring.IsChecked = Subject.Term == Term.Spring;
            completionComboBox.SelectedIndex = (int)Subject.CompletionType;
            Subject updatedSubject = new Subject(
                Subject.Id,
                codeTextBox.Text,
                nameTextBox.Text,
                int.Parse(yearTextBox.Text),
                (bool)radioTermSpring.IsChecked ? Term.Spring : Term.Autumn,
                int.Parse(creditTextBox.Text),
                int.Parse(durationTextBox.Text),
                (CompletionType)completionComboBox.SelectedIndex,
                prerequisitesTextBox.Text
                );
            Dao.update(updatedSubject);
            DialogResult = true;
        }

    }
}
