﻿using SchoolEvidence.Entities;
using SchoolEvidence.Entities.Structs;
using SchoolEvidence.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;

namespace SchoolEvidence.Dialogs
{
    /// <summary>
    /// Interaction logic for AddGroupDialog.xaml
    /// </summary>
    public partial class AddGroupDialog : Window
    {
        private readonly MainWindow Main;
        private readonly Subject This_Subject;
        private readonly int TermStartMonth;
        private readonly int TermWeeks;
        public AddGroupDialog(MainWindow main, Subject this_subject)
        {
            InitializeComponent();
            Main = main;
            This_Subject = this_subject;
            Title = "Add group to " + This_Subject.Name;
            TermStartMonth = This_Subject.Term == Term.Spring ? Main.SpringTermStartMonth : Main.AutumnTermStartMonth;
            TermWeeks = Main.TermWeeks;
            dayCombo.ItemsSource = Enum.GetValues(typeof(DayOfWeek));
            hourCombo.ItemsSource = Enumerable.Range(1, 24);
        }

        private void cancelBtn_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

        private void addBtn_Click(object sender, RoutedEventArgs e)
        {
            if (!CheckValues()) return;
            GroupStruct newGroup = new GroupStruct(
                    nameTextBox.Text.Trim(),
                    (DayOfWeek)dayCombo.SelectedIndex,
                    new TimeSpan((int)hourCombo.SelectedValue, 0, 0),
                    This_Subject.Id
                    );
            List<int> tempGroupList = Main.Group_Dao.findAll().Select(g => g.Id).ToList();
            Main.Group_Dao.insert(newGroup);
            Group addedGroup = Main.Group_Dao.findAll().First(g => !tempGroupList.Contains(g.Id));
            addClassesForTerm(addedGroup);
            DialogResult = true;
        }

        private void addClassesForTerm(Group addedGroup)
        {
            DateTime tempFrom = DateTime.Parse(string.Format("{0}-{1}-01", This_Subject.Year, TermStartMonth)).DayOfWeek == DayOfWeek.Monday
                ? DateTime.Parse(string.Format("{0}-{1}-01", This_Subject.Year, TermStartMonth))
                : DateTime.Parse(string.Format("{0}-{1}-01", This_Subject.Year, TermStartMonth)).AddDays((7 - (int)DateTime.Parse(string.Format("{0}-{1}-01", This_Subject.Year, TermStartMonth)).DayOfWeek) + 1);
            DateTime tempTo = tempFrom.AddDays(7 * (TermWeeks - 1));
            int daysToAdd = ((int)addedGroup.Day - (int)tempFrom.DayOfWeek + 7) % 7;

            do
            {
                tempFrom = tempFrom.AddDays(daysToAdd);
                Main.Class_Dao.insert(new ClassStruct(tempFrom.Add(addedGroup.Time), addedGroup.Id));
                daysToAdd = 7;
            } while (tempFrom < tempTo);

            return;
        }

        private bool CheckValues()
        {
            bool returnBool = true;
            if (Utils.IsNullOrEmptyOrBlank(nameTextBox.Text)) { nameTextBox.BorderBrush = Utils.warningColor; returnBool = false; }
            if (dayCombo.SelectedIndex == -1) { dayCombo.BorderBrush = Utils.warningColor; returnBool = false; }
            if (hourCombo.SelectedIndex == -1) { hourCombo.BorderBrush = Utils.warningColor; returnBool = false; }
            if (Main.Group_Dao.findAll().Select(g => g.Name).Contains(nameTextBox.Text.Trim()))
            {
                MessageBox.Show("Group with this name already exists.");
                returnBool = false;
            };
            return returnBool;
        }
    }
}
