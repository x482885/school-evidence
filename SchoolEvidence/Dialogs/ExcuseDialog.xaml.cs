﻿using SchoolEvidence.Entities;
using SchoolEvidence.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace SchoolEvidence.Dialogs
{
    /// <summary>
    /// Interaction logic for ExcuseDialog.xaml
    /// </summary>
    public partial class ExcuseDialog : Window
    {
        private readonly MainWindow Main;
        private readonly Student This_Student;
        public ExcuseDialog(MainWindow main, Student this_student)
        {
            InitializeComponent();
            Main = main;
            This_Student = this_student;
            Title = string.Format("Add excuse to {1}.{0}", This_Student.Surname, This_Student.Name.Substring(0, 1));
        }

        private void cancelBtn_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

        private void addBtn_Click(object sender, RoutedEventArgs e)
        {
            if (!CheckValues()) return;
            ExcuseStruct addedStruct = new ExcuseStruct(
                    (DateTime)dateFromPicker.SelectedDate,
                    (DateTime)dateToPicker.SelectedDate + new TimeSpan(23,59,59),
                    reasonTextBox.Text.Trim(),
                    This_Student.Id
                    );
            List<int> tempListIds = Main.Excuse_Dao.findAll().Select(ex => ex.Id).ToList();
            Main.Excuse_Dao.insert(addedStruct);
            Excuse insertedExcuse = Main.Excuse_Dao.findAll().First(i => !tempListIds.Contains(i.Id));
            foreach (StudentClass item in Main.StudentClass_Dao.findAll().Where(i => i.StudentId == This_Student.Id && i.ExcuseId == null && Main.Class_Dao.findAll().First(c => c.Id == i.ClassId).DateTime >= insertedExcuse.DateFrom && Main.Class_Dao.findAll().First(c => c.Id == i.ClassId).DateTime <= insertedExcuse.DateTo))
                Main.StudentClass_Dao.update(new StudentClass(item.StudentId, item.ClassId, item.Attended, insertedExcuse.Id));
            DialogResult = true;
        }

        private bool CheckValues()
        {
            bool returnBool = true;
            if (dateFromPicker.SelectedDate == null) { dateFromPicker.BorderBrush = Utils.warningColor; returnBool = false; }
            if (dateToPicker.SelectedDate == null) { dateToPicker.BorderBrush = Utils.warningColor; returnBool = false; }
            if (Utils.IsNullOrEmptyOrBlank(reasonTextBox.Text)) { reasonTextBox.BorderBrush = Utils.warningColor; returnBool = false; }
            return returnBool;
        }

        private void dateFromPicker_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            if (!dateFromPicker.SelectedDate.HasValue) return;
            dateToPicker.DisplayDateStart = dateFromPicker.SelectedDate.Value;
        }
    }
}
