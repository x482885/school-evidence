﻿using SchoolEvidence.DAO;
using SchoolEvidence.Entities;
using SchoolEvidence.Tools;
using System;
using System.Windows;

namespace SchoolEvidence.Dialogs
{
    /// <summary>
    /// Interaction logic for AddStudentDialog.xaml
    /// </summary>
    public partial class AddStudentDialog : Window
    {
        private readonly StudentDao Dao;
        private readonly bool Editing = false;
        private readonly Student Student = null;
        public AddStudentDialog(StudentDao dao)
        {
            InitializeComponent();
            Title = "Add student";
            Dao = dao;
        }
        public AddStudentDialog(StudentDao dao, bool editing, Student student) : this(dao)
        {
            Editing = editing;
            Student = student;
            addBtn.Content = "Confirm";
            Title = string.Format("Edit {0}.{1}", student.Name.Substring(0, 1), student.Surname);
            nameTextBox.Text = Student.Name;
            surnameTextBox.Text = Student.Surname;
            datePicker.SelectedDate = Student.DateOfBirth;
            radioGenderMale.IsChecked = Student.Gender == Gender.male;
            radioGenderFemale.IsChecked = Student.Gender == Gender.female;
            addressTextBox.Text = Student.Address;
            emailTextBox.Text = Student.Email;
        }

        private bool CheckValues()
        {
            bool returnBool = true;
            if (Utils.IsNullOrEmptyOrBlank(nameTextBox.Text)) { nameTextBox.BorderBrush = Utils.warningColor; returnBool = false; }
            if (Utils.IsNullOrEmptyOrBlank(surnameTextBox.Text)) { surnameTextBox.BorderBrush = Utils.warningColor; returnBool = false; }
            if (Utils.IsNullOrEmptyOrBlank(emailTextBox.Text) || emailTextBox.Text.Length < 3) { emailTextBox.BorderBrush = Utils.warningColor; returnBool = false; }
            if (Utils.IsNullOrEmptyOrBlank(addressTextBox.Text)) { addressTextBox.BorderBrush = Utils.warningColor; returnBool = false; }
            if (radioGenderMale.IsChecked == null || radioGenderFemale.IsChecked == null) returnBool = false;
            if (datePicker.SelectedDate == null) { datePicker.BorderBrush = Utils.warningColor; returnBool = false; }
            return returnBool;
        }

        private void addBtn_Click(object sender, RoutedEventArgs e)
        {
            if (!CheckValues()) return;
            if (Editing) { Edit(); return; }
            Dao.insert(new StudentStruct(
                nameTextBox.Text.Trim(),
                surnameTextBox.Text.Trim(),
                (DateTime)datePicker.SelectedDate,
                (bool)radioGenderMale.IsChecked ? Gender.male : Gender.female,
                addressTextBox.Text.Trim(),
                emailTextBox.Text.Trim()
                ));
            DialogResult = true;
        }

        private void Edit()
        {
            Student updatedStudent = new Student(
                Student.Id,
                nameTextBox.Text.Trim(),
                surnameTextBox.Text.Trim(),
                (DateTime)datePicker.SelectedDate,
                (bool)radioGenderMale.IsChecked ? Gender.male : Gender.female,
                addressTextBox.Text.Trim(),
                emailTextBox.Text.Trim()
                );
            Dao.update(updatedStudent);
            DialogResult = true;
        }

        private void cancelBtn_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

    }
}
