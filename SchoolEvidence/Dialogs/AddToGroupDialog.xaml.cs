﻿using SchoolEvidence.Entities;
using SchoolEvidence.Tools;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace SchoolEvidence.Dialogs
{
    /// <summary>
    /// Interaction logic for AddToGroupDialog.xaml
    /// </summary>
    public partial class AddToGroupDialog : Window
    {
        private readonly MainWindow Main;
        private readonly Student This_Student;
        private readonly List<Subject> subjects = new List<Subject>();
        private List<Group> groups = new List<Group>();
        public AddToGroupDialog(MainWindow main, Student this_student)
        {
            InitializeComponent();
            Main = main;
            This_Student = this_student;
            Title = string.Format("Add {0}.{1} to group", This_Student.Name.Substring(0, 1), This_Student.Surname);
            subjects = Main.Subject_Dao.findAll().ToList();
            subjectCombo.ItemsSource = subjects.Select(i => i.Code);
        }

        private void addBtn_Click(object sender, RoutedEventArgs e)
        {
            if (!CheckValues()) return;
            addToGroupOperation(This_Student, Main, groups.ElementAt(groupCombo.SelectedIndex));
            DialogResult = true;
        }

        private void cancelBtn_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }
        private bool CheckValues()
        {
            bool returnBool = true;
            if (subjectCombo.SelectedIndex == -1) { subjectCombo.BorderBrush = Utils.warningColor; returnBool = false; }
            if (groupCombo.SelectedIndex == -1) { groupCombo.BorderBrush = Utils.warningColor; returnBool = false; }
            return returnBool;
        }

        private void subjectCombo_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (subjectCombo.SelectedIndex == -1 || subjects.Count < 1) { enableComboButton(false, false); return; }
            groups = Main.Group_Dao.findAll().Where(i => i.SubjectId == subjects.ElementAt(subjectCombo.SelectedIndex).Id).ToList();
            groupCombo.ItemsSource = groups.Select(i => i.Name);
            enableComboButton(true, false);

        }

        private void groupCombo_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (groupCombo.SelectedIndex == -1 || groups.Count < 1) { enableComboButton(true, false); return; }
            enableComboButton(true, true);
        }

        private void enableComboButton(bool combo, bool button)
        {
            groupCombo.IsEnabled = combo;
            addBtn.IsEnabled = button;
        }

        public static void addToGroupOperation(Student student, MainWindow Main, Group group)
        {
            Main.GroupStudent_Dao.insert(new GroupStudent(student.Id, group.Id));
            foreach (Class c in Main.Class_Dao.findAll().Where(c => c.GroupId == group.Id))
            {
                StudentClass toInsert = new StudentClass(student.Id, c.Id);
                if (Main.Excuse_Dao.findAll().Any(e => e.StudentID == student.Id && e.DateFrom <= c.DateTime && e.DateTo >= c.DateTime))
                {
                    int excuseId = Main.Excuse_Dao.findAll().First(e => e.StudentID == student.Id && e.DateFrom <= c.DateTime && e.DateTo >= c.DateTime).Id;
                    toInsert = new StudentClass(student.Id, c.Id, false, excuseId);
                }
                Main.StudentClass_Dao.insert(toInsert);
            }
        }

    }
}
