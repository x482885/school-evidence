﻿using SchoolEvidence.Entities;
using SchoolEvidence.Views;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace SchoolEvidence.Dialogs
{
    /// <summary>
    /// Interaction logic for AddStudentsToGroupDialog.xaml
    /// </summary>
    public partial class AddStudentsToGroupDialog : Window
    {
        private readonly MainWindow Main;
        private readonly Group This_group;
        private readonly bool Removing = false;
        public AddStudentsToGroupDialog(MainWindow main, Group group)
        {
            InitializeComponent();
            Main = main;
            This_group = group;
            ConfirmBtn.IsEnabled = false;
            Title = "Add students to group " + This_group.Name;
            StudentList.ItemsSource = Main.Student_Dao.findAll().Where(s => !Main.GroupStudent_Dao.findAll().Any(gs => gs.GroupId == group.Id && gs.StudentId == s.Id));
        }

        public AddStudentsToGroupDialog(MainWindow main, Group group, bool removing) : this(main, group)
        {
            Removing = removing;
            ConfirmBtn.Content = "Remove";
            CollumnAdd.Header = "Remove";
            CollumnAdd.Width = 70;
            StudentList.ItemsSource = Main.Student_Dao.findAll().Where(s => Main.GroupStudent_Dao.findAll().Any(gs => gs.GroupId == group.Id && gs.StudentId == s.Id));
        }

        private void CancelBtn_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

        private void ConfirmBtn_Click(object sender, RoutedEventArgs e)
        {
            if (Removing) { RemoveOperation(); return; }
            foreach (Student s in StudentList.SelectedItems)
            {
                AddToGroupDialog.addToGroupOperation(s, Main, This_group);
            }
            DialogResult = true;
        }

        private void RemoveOperation()
        {
            if (StudentList.SelectedItems.Count == 0) { DialogResult = false; return; }
            foreach (Student s in StudentList.SelectedItems)
            {
                StudentView.removeFromGroupOperation(s, This_group, Main);
            }
            DialogResult = true;
        }

        private void StudentList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (StudentList.SelectedItems.Count == 0) { ConfirmBtn.IsEnabled = false; return; }
            ConfirmBtn.IsEnabled = true;
        }
    }
}
