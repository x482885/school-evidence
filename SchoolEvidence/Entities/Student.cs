﻿using System;

namespace SchoolEvidence.Entities
{
    public enum Gender
    {
        male, female
    }
    public class Student
    {
        public int Id { get; set; }
        public string Name { get; private set; }
        public string Surname { get; private set; }
        public DateTime DateOfBirth { get; private set; }
        public Gender Gender { get; private set; }
        public string Address { get; private set; }
        public string Email { get; private set; }

        public Student(int id, string name, string surname, DateTime dateOfBirth, Gender gender, string address, string email)
        {
            Id = id;
            Name = name;
            Surname = surname;
            DateOfBirth = dateOfBirth;
            Gender = gender;
            Address = address;
            Email = email;
        }

    }
}
