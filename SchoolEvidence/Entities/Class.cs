﻿using System;

namespace SchoolEvidence.Entities
{
    public class Class
    {
        public int Id { get; private set; }
        public DateTime DateTime { get; private set; }
        public int GroupId { get; private set; }
        public Class(int id, DateTime dateTime, int groupId)
        {
            Id = id;
            DateTime = dateTime;
            GroupId = groupId;
        }
    }
}
