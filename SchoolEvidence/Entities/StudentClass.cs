﻿namespace SchoolEvidence.Entities
{
    public class StudentClass
    {

        public int StudentId { get; set; }
        public int ClassId { get; set; }
        public bool Attended { get; set; }
        public int? ExcuseId { get; set; }

        public StudentClass(int studentId, int classId, bool attended = false, int? excuseId = null)
        {
            StudentId = studentId;
            ClassId = classId;
            Attended = attended;
            ExcuseId = excuseId;
        }

    }
}
