﻿using System;

namespace SchoolEvidence.Entities
{
    public struct StudentStruct
    {

        public string Name { get; set; }
        public string Surname { get; set; }
        public DateTime DateOfBirth { get; set; }
        public Gender Gender { get; set; }
        public string Address { get; set; }
        public string Email { get; set; }

        public StudentStruct(string name, string surname, DateTime dateOfBirth, Gender gender, string address, string email)
        {
            Name = name;
            Surname = surname;
            DateOfBirth = dateOfBirth;
            Gender = gender;
            Address = address;
            Email = email;
        }

    }
}
