﻿using System;

namespace SchoolEvidence.Entities
{
    public struct ExcuseStruct
    {
        public int StudentID { get; set; }
        public DateTime DateFrom { get; set; }
        public DateTime DateTo { get; set; }
        public string Reason { get; set; }

        public ExcuseStruct(DateTime dateFrom, DateTime dateTo, string reason, int studentID)
        {
            DateFrom = dateFrom;
            DateTo = dateTo;
            Reason = reason;
            StudentID = studentID;
        }

    }
}
