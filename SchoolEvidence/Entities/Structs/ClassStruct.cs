﻿using System;

namespace SchoolEvidence.Entities
{
    public struct ClassStruct
    {
        public DateTime DateTime { get; private set; }
        public int GroupId { get; private set; }

        public ClassStruct(DateTime dateTime, int groupId)
        {
            DateTime = dateTime;
            GroupId = groupId;
        }

    }
}
