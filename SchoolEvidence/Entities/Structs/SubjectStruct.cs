﻿namespace SchoolEvidence.Entities
{
    public struct SubjectStruct
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public int Year { get; set; }
        public Term Term { get; set; }
        public int Credit { get; set; }
        public int DurationHours { get; set; }
        public CompletionType CompletionType { get; set; }
        public string Prerequisites { get; set; }

        public SubjectStruct(string code, string name, int year, Term term, int credit, int durationHours, CompletionType completionType, string prerequisites)
        {
            Code = code;
            Name = name;
            Year = year;
            Term = term;
            Credit = credit;
            DurationHours = durationHours;
            CompletionType = completionType;
            Prerequisites = prerequisites;
        }

    }
}
