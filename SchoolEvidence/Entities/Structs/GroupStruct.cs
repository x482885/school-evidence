﻿using System;

namespace SchoolEvidence.Entities.Structs
{
    public struct GroupStruct
    {
        public string Name { get; private set; }
        public DayOfWeek Day { get; private set; }
        public TimeSpan Time { get; private set; }
        public int SubjectId { get; private set; }
        public GroupStruct(string name, DayOfWeek day, TimeSpan time, int subjectId)
        {
            Name = name;
            Day = day;
            Time = time;
            SubjectId = subjectId;
        }

    }
}
