﻿using System;

namespace SchoolEvidence.Entities
{
    public class Group
    {
        public int Id { get; private set; }
        public string Name { get; private set; }
        public DayOfWeek Day { get; private set; }
        public TimeSpan Time { get; private set; }
        public int SubjectId { get; private set; }
        public Group(int id, string name, DayOfWeek day, TimeSpan time, int subjectId)
        {
            Id = id;
            Name = name;
            Day = day;
            Time = time;
            SubjectId = subjectId;
        }

    }
}
