﻿namespace SchoolEvidence.Entities
{
    public class GroupStudent
    {

        public int StudentId { get; set; }
        public int GroupId { get; set; }

        public GroupStudent(int studentId, int groupId)
        {
            StudentId = studentId;
            GroupId = groupId;
        }

    }
}
