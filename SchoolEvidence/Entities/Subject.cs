﻿namespace SchoolEvidence.Entities
{
    public enum CompletionType
    {
        z, k, zk, SoZk, SZk, SRZk, SDZk
    }

    public enum Term
    {
        Spring, Autumn
    }
    public class Subject
    {
        public int Id { get; private set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public int Year { get; set; }
        public Term Term { get; set; }
        public int Credit { get; set; }
        public int DurationHours { get; set; }
        public CompletionType CompletionType { get; set; }
        public string Prerequisites { get; set; }

        public Subject(int id, string code, string name, int year, Term term, int credit, int durationHours, CompletionType completionType, string prerequisites)
        {
            Id = id;
            Code = code;
            Name = name;
            Year = year;
            Term = term;
            Credit = credit;
            DurationHours = durationHours;
            CompletionType = completionType;
            Prerequisites = prerequisites;
        }

    }
}
