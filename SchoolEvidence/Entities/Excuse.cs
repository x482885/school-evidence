﻿using System;

namespace SchoolEvidence.Entities
{
    public class Excuse
    {
        public int Id { get; private set; }
        public int StudentID { get; private set; }
        public DateTime DateFrom { get; private set; }
        public DateTime DateTo { get; private set; }
        public string Reason { get; private set; }
        public Excuse(int id, int studentID, DateTime dateFrom, DateTime dateTo, string reason)
        {
            Id = id;
            StudentID = studentID;
            DateFrom = dateFrom;
            DateTo = dateTo;
            Reason = reason;
        }
    }
}
