﻿using SchoolEvidence.Dialogs;
using SchoolEvidence.Entities;
using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace SchoolEvidence.Views
{
    /// <summary>
    /// Interaction logic for GroupView.xaml
    /// </summary>
    public partial class GroupView : UserControl, ViewInterface
    {

        private readonly MainWindow Main;
        private readonly Group This_Group;
        public GroupView(MainWindow main, Group this_group)
        {
            InitializeComponent();
            Main = main;
            This_Group = this_group;
            GroupTitle.Text = "Group " + This_Group.Name;
            UpdateUI();
        }

        public void UpdateUI()
        {
            ClassList.ItemsSource = Main.Class_Dao.findAll().Where(i => i.GroupId == This_Group.Id).Select(i => new Tuple<Class, int>(i, Main.StudentClass_Dao.findAll().Where(c => c.ClassId == i.Id).Count()));
        }

        private void ClassList_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (ClassList.SelectedIndex == -1) return;
            Main.SwitchContext(new ClassView(Main, ((Tuple<Class, int>)ClassList.SelectedItem).Item1));
        }

        private void addStudentsBtn_Click(object sender, RoutedEventArgs e)
        {
            AddStudentsToGroupDialog dialog = new AddStudentsToGroupDialog(Main, This_Group);
            dialog.ShowDialog();
            UpdateUI();
        }

        private void removeStudentsBtn_Click(object sender, RoutedEventArgs e)
        {
            AddStudentsToGroupDialog dialog = new AddStudentsToGroupDialog(Main, This_Group, true);
            dialog.ShowDialog();
            UpdateUI();
        }
    }
}
