﻿using SchoolEvidence.Dialogs;
using SchoolEvidence.Entities;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace SchoolEvidence.Views
{
    /// <summary>
    /// Interaction logic for StudentsView.xaml
    /// </summary>
    public partial class StudentsView : UserControl, ViewInterface
    {
        private readonly MainWindow Main;
        public StudentsView(MainWindow main)
        {
            InitializeComponent();
            Main = main;
            UpdateUI();
        }

        public void UpdateUI()
        {
            StudentList.ItemsSource = Main.Student_Dao.findAll();
        }

        private void AddBtn_Click(object sender, RoutedEventArgs e)
        {
            AddStudentDialog dialog = new AddStudentDialog(Main.Student_Dao);
            if (dialog.ShowDialog() != true) return;
            UpdateUI();
        }

        private void ExcuseBtn_Click(object sender, RoutedEventArgs e)
        {
            if (StudentList.SelectedIndex == -1) return;
            ExcuseDialog dialog = new ExcuseDialog(Main, (Student)StudentList.SelectedItem);
            if (dialog.ShowDialog() != true) return;
        }

        private void EditBtn_Click(object sender, RoutedEventArgs e)
        {
            if (StudentList.SelectedIndex == -1) return;
            AddStudentDialog dialog = new AddStudentDialog(Main.Student_Dao, true, (Student)StudentList.SelectedItem);
            if (dialog.ShowDialog() != true) return;
            UpdateUI();
        }

        private void RemoveBtn_Click(object sender, RoutedEventArgs e)
        {
            if (StudentList.SelectedIndex == -1) return;
            Student studentToDelete = (Student)StudentList.SelectedItem;
            Main.GroupStudent_Dao.findAll().Where(g => g.StudentId == studentToDelete.Id).ToList().ForEach(i => Main.GroupStudent_Dao.delete(i));
            Main.StudentClass_Dao.findAll().Where(sc => sc.StudentId == studentToDelete.Id).ToList().ForEach(i => Main.StudentClass_Dao.delete(i));
            Main.Excuse_Dao.findAll().Where(ex => ex.StudentID == studentToDelete.Id).ToList().ForEach(i => Main.Excuse_Dao.delete(i));
            Main.Student_Dao.delete(studentToDelete);
            UpdateUI();
        }

        private void StudentList_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (StudentList.SelectedIndex == -1) return;
            Main.SwitchContext(new StudentView(Main, (Student)StudentList.SelectedItem));
        }
    }
}
