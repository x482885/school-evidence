﻿using SchoolEvidence.Dialogs;
using SchoolEvidence.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace SchoolEvidence.Views
{
    /// <summary>
    /// Interaction logic for StudentView.xaml
    /// </summary>
    public partial class StudentView : UserControl, ViewInterface
    {
        private readonly MainWindow Main;
        private readonly Student This_Student;
        public StudentView(MainWindow main, Student this_student)
        {
            InitializeComponent();
            Main = main;
            This_Student = this_student;
            UpdateUI();
        }

        public void UpdateUI()
        {
            NameTitle.Text = string.Format("{0} {1}", This_Student.Name, This_Student.Surname);
            GroupList.ItemsSource = Main.Group_Dao.findAll()
                .Where(i => Main.GroupStudent_Dao.findAll()
                    .Where(s => s.StudentId == This_Student.Id)
                    .Select(p => p.GroupId).Contains(i.Id)
                ).Select(i => new Tuple<string, Group>(Main.Subject_Dao.findAll().First(s => s.Id == i.SubjectId).Code, i));
            DateValue.Text = This_Student.DateOfBirth.Date.ToShortDateString();
            GenderValue.Text = This_Student.Gender.ToString();
            AddressValue.Text = This_Student.Address;
            EmailValue.Text = This_Student.Email;
            int total = Main.StudentClass_Dao.findAll().Where(i => i.StudentId == This_Student.Id).Count();
            int attended = Main.StudentClass_Dao.findAll().Where(i => i.StudentId == This_Student.Id && i.Attended).Count();
            int excused = Main.StudentClass_Dao.findAll().Where(i => i.StudentId == This_Student.Id && !i.Attended && i.ExcuseId != null).Count();
            int missed = total - attended;
            int absent = missed - excused;
            MissedValue.Text = string.Format("{0}/{1}", missed, total);
            ExcusedValue.Text = string.Format("{0}/{1}", excused, total);
            AbsentValue.Text = string.Format("{0}/{1}", absent, total);
        }

        private void AddBtn_Click(object sender, RoutedEventArgs e)
        {
            AddToGroupDialog dialog = new AddToGroupDialog(Main, This_Student);
            if (dialog.ShowDialog() != true) return;
            UpdateUI();
        }

        private void RemoveBtn_Click(object sender, RoutedEventArgs e)
        {
            if (GroupList.SelectedIndex == -1) return;
            removeFromGroupOperation(This_Student, ((Tuple<string, Group>)GroupList.SelectedItem).Item2, Main);
            UpdateUI();
        }

        private void ExcuseBtn_Click(object sender, RoutedEventArgs e)
        {
            ExcuseDialog dialog = new ExcuseDialog(Main, This_Student);
            if (dialog.ShowDialog() != true) return;
            UpdateUI();
        }

        private void EditBtn_Click(object sender, RoutedEventArgs e)
        {
            AddStudentDialog dialog = new AddStudentDialog(Main.Student_Dao, true, This_Student);
            if (dialog.ShowDialog() != true) return;
            UpdateUI();
        }

        private void GroupList_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (GroupList.SelectedIndex == -1) return;
            Main.SwitchContext(new GroupView(Main, ((Tuple<string, Group>)GroupList.SelectedItem).Item2));
        }

        public static void removeFromGroupOperation(Student student, Group group, MainWindow Main)
        {
            GroupStudent groupToDelete = Main.GroupStudent_Dao.findAll().First(g => g.GroupId == group.Id && student.Id == g.StudentId);
            List<StudentClass> studentClassesToDelete = Main.StudentClass_Dao.findAll().Where(sc => sc.StudentId == student.Id && Main.Class_Dao.findAll().Where(c => c.GroupId == groupToDelete.GroupId).Select(c => c.Id).Contains(sc.ClassId)).ToList();
            studentClassesToDelete.ForEach(sc => Main.StudentClass_Dao.delete(sc));
            Main.GroupStudent_Dao.delete(groupToDelete);
        }
    }
}
