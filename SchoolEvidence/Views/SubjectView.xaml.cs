﻿using SchoolEvidence.Dialogs;
using SchoolEvidence.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace SchoolEvidence.Views
{
    /// <summary>
    /// Interaction logic for SubjectView.xaml
    /// </summary>
    public partial class SubjectView : UserControl, ViewInterface
    {
        private readonly Subject This_Subject;
        private readonly MainWindow Main;
        public SubjectView(MainWindow main, Subject this_subject)
        {
            InitializeComponent();
            Main = main;
            This_Subject = this_subject;
            UpdateUI();
        }

        private void AddGroupBtn_Click(object sender, RoutedEventArgs e)
        {
            if (This_Subject.Year < DateTime.Now.Year) { MessageBox.Show("Cannot add group to subject from past."); return; }
            AddGroupDialog dialog = new AddGroupDialog(Main, This_Subject);
            if (dialog.ShowDialog() == false) return;
            UpdateUI();

        }

        private void RemoveGroupBtn_Click(object sender, RoutedEventArgs e)
        {
            if (GroupsList.SelectedIndex == -1) return;
            Group selectedGroup = (Group)GroupsList.SelectedItem;
            List<GroupStudent> groupStudentsToDelete = Main.GroupStudent_Dao.findAll().Where(gs => gs.GroupId == selectedGroup.Id).ToList();
            List<Class> classesToDelete = Main.Class_Dao.findAll().Where(c => c.GroupId == selectedGroup.Id).ToList();
            List<StudentClass> studentClassesToDelete = Main.StudentClass_Dao.findAll().Where(sc => classesToDelete.Select(c => c.Id).Contains(sc.ClassId)).ToList();
            groupStudentsToDelete.ForEach(gs => Main.GroupStudent_Dao.delete(gs));
            studentClassesToDelete.ForEach(sc => Main.StudentClass_Dao.delete(sc));
            classesToDelete.ForEach(c => Main.Class_Dao.delete(c));
            Main.Group_Dao.delete((Group)GroupsList.SelectedItem);
            UpdateUI();

        }

        public void UpdateUI()
        {
            SubjectTitle.Text = This_Subject.Name;
            GroupsList.ItemsSource = Main.Group_Dao.findAll().Where(g => g.SubjectId == This_Subject.Id);
            StudentsList.ItemsSource =
                Main.Student_Dao.findAll().Where(st =>
                    Main.GroupStudent_Dao.findAll()
                        .Where(gs => Main.Group_Dao.findAll()
                            .Where(g => This_Subject.Id == g.SubjectId)
                    .Select(g => g.Id)
                    .Contains(gs.GroupId))
                .Select(gs => gs.StudentId)
                .Contains(st.Id))
                .Distinct();
        }

        private void GroupsList_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (GroupsList.SelectedIndex == -1) return;
            Main.SwitchContext(new GroupView(Main, (Group)GroupsList.SelectedItem));
        }

        private void StudentsList_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (StudentsList.SelectedIndex == -1) return;
            Main.SwitchContext(new StudentView(Main, (Student)StudentsList.SelectedItem));
        }
    }
}
