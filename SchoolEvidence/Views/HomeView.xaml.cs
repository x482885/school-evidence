﻿using SchoolEvidence.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Controls;
using System.Windows.Input;
using static SchoolEvidence.DatabaseDataSet;

namespace SchoolEvidence.Views
{
    /// <summary>
    /// Interaction logic for HomeView.xaml
    /// </summary>
    public partial class HomeView : UserControl, ViewInterface
    {
        private readonly MainWindow Main;
        public HomeView(MainWindow main)
        {
            InitializeComponent();
            Main = main;
            UpdateUI();
        }

        public void UpdateUI()
        {
            List<int> todayClassedId = Main.Class_Dao.findAll().Where(i => i.DateTime.Date == DateTime.Today).Select(i => i.Id).ToList();
            if (todayClassedId.Count < 1) return;
            StudentList.ItemsSource = Main.StudentClass_Dao.findAll().Where(i => todayClassedId.Contains(i.ClassId));
            ClassList.ItemsSource = Main.Class_Dao.findAll().Where(i => todayClassedId.Contains(i.Id)).Select(i => new Tuple<SubjectSetRow, GroupSetRow, Class>(Main.Subject_Dao.adapter.GetData().FindById(Main.Group_Dao.adapter.GetData().FindById(i.GroupId).Id), Main.Group_Dao.adapter.GetData().FindById(i.GroupId), i)).ToList();
        }

        private void ClassList_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (ClassList.SelectedIndex == -1) return;
            Main.SwitchContext(new ClassView(Main, ((Tuple<SubjectSetRow, GroupSetRow, Class>)ClassList.SelectedItem).Item3));
        }

        private void StudentList_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (StudentList.SelectedIndex == -1) return;
            Main.SwitchContext(new StudentView(Main, (Student)StudentList.SelectedItem));
        }
    }
}
