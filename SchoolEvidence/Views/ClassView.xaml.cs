﻿using SchoolEvidence.Entities;
using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace SchoolEvidence.Views
{
    /// <summary>
    /// Interaction logic for ClassView.xaml
    /// </summary>
    public partial class ClassView : UserControl, ViewInterface
    {
        private readonly MainWindow Main;
        private readonly Class This_Class;
        public ClassView(MainWindow main, Class this_class)
        {
            InitializeComponent();
            Main = main;
            This_Class = this_class;
            UpdateUI();
        }

        public void UpdateUI()
        {
            StudentList.ItemsSource = Main.StudentClass_Dao.findAll().Where(sc => sc.ClassId == This_Class.Id).Select(sc => new Tuple<Student, StudentClass>(Main.Student_Dao.findAll().First(s => s.Id == sc.StudentId), sc));
        }

        private void CancelBtn_Click(object sender, RoutedEventArgs e)
        {
            UpdateUI();
        }

        private void ConfirmBtn_Click(object sender, RoutedEventArgs e)
        {
            foreach (Tuple<Student, StudentClass> item in StudentList.Items) Main.StudentClass_Dao.update(item.Item2);
        }

        private void StudentList_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (StudentList.SelectedIndex == -1) return;
            Main.SwitchContext(new StudentView(Main, ((Tuple<Student, StudentClass>)StudentList.SelectedItem).Item1));
        }
    }
}
