﻿using SchoolEvidence.Dialogs;
using SchoolEvidence.Entities;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace SchoolEvidence.Views
{
    /// <summary>
    /// Interaction logic for SubjectsView.xaml
    /// </summary>
    public partial class SubjectsView : UserControl, ViewInterface
    {
        private readonly MainWindow Main;
        public SubjectsView(MainWindow main)
        {
            InitializeComponent();
            Main = main;
            SubjectsList.ItemsSource = Main.Subject_Dao.findAll();
        }

        private void AddBtn_Click(object sender, RoutedEventArgs e)
        {
            AddSubjectDialog dialog = new AddSubjectDialog(Main.Subject_Dao);
            if (dialog.ShowDialog() != true) return;
            UpdateUI();
        }

        private void RemoveBtn_Click(object sender, RoutedEventArgs e)
        {
            if (SubjectsList.SelectedIndex == -1) return;
            Subject subjectToDelete = (Subject)SubjectsList.SelectedItem;
            List<Group> groupsToDelete = Main.Group_Dao.findAll().Where(g => g.SubjectId == subjectToDelete.Id).ToList();
            List<GroupStudent> groupStudentsToDelete = Main.GroupStudent_Dao.findAll().Where(gs => groupsToDelete.Select(g => g.Id).Contains(gs.GroupId)).ToList();
            List<Class> classesToDelete = Main.Class_Dao.findAll().Where(c => groupsToDelete.Select(g => g.Id).Contains(c.GroupId)).ToList();
            List<StudentClass> studentClassesToDelete = Main.StudentClass_Dao.findAll().Where(sc => classesToDelete.Select(c => c.Id).Contains(sc.ClassId)).ToList();
            studentClassesToDelete.ForEach(sc => Main.StudentClass_Dao.delete(sc));
            groupStudentsToDelete.ForEach(gs => Main.GroupStudent_Dao.delete(gs));
            classesToDelete.ForEach(c => Main.Class_Dao.delete(c));
            groupsToDelete.ForEach(g => Main.Group_Dao.delete(g));
            Main.Subject_Dao.delete(subjectToDelete);
            UpdateUI();
        }

        public void UpdateUI()
        {
            SubjectsList.ItemsSource = Main.Subject_Dao.findAll();
        }

        private void SubjectsList_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (SubjectsList.SelectedIndex == -1) return;
            Main.SwitchContext(new SubjectView(Main, (Subject)SubjectsList.SelectedItem));
        }
    }
}
