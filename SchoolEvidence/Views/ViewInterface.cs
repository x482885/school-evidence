﻿namespace SchoolEvidence.Views
{
    public interface ViewInterface
    {
        void UpdateUI();
    }
}
