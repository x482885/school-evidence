﻿using System.Collections.Generic;

namespace SchoolEvidence.DAO
{
    internal interface DataAcessObject<T, U, S>
    {

        void insert(S strct);
        ICollection<T> findAll();
        void update(T entity);
        void delete(T entity);
        T toEntity(U row);

    }
}
