﻿using SchoolEvidence.Entities;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using static SchoolEvidence.DatabaseDataSet;

namespace SchoolEvidence.DAO
{
    public class StudentDao : DataAcessObject<Student, StudentSetRow, StudentStruct>
    {
        public DatabaseDataSetTableAdapters.StudentSetTableAdapter adapter { get; private set; } = new DatabaseDataSetTableAdapters.StudentSetTableAdapter();
        public void insert(StudentStruct strct)
        {
            adapter.Insert(
                strct.Name,
                strct.Surname,
                strct.DateOfBirth,
                (int)strct.Gender,
                strct.Address,
                strct.Email
                );
        }

        public void delete(Student entity)
        {
            if (!adapter.GetData().Any(row => row.Id == entity.Id)) return;
            StudentSetRow tempDelete = adapter.GetData().FindById(entity.Id);
            adapter.Delete(
                tempDelete.Id,
                tempDelete.DateOfBirth,
                tempDelete.Gender
                );
        }

        public ICollection<Student> findAll()
        {
            return adapter.GetData().Select(row => toEntity(row)).ToList();
        }

        public Student toEntity(StudentSetRow row)
        {
            return new Student(
                row.Id,
                row.Name,
                row.Surname,
                row.DateOfBirth,
                (Gender)row.Gender,
                row.Address,
                row.Email
                );
        }

        public void update(Student entity)
        {
            if (!adapter.GetData().Any(row => row.Id == entity.Id)) return;
            StudentSetRow tempUpdate = adapter.GetData().FindById(entity.Id);
            tempUpdate.Name = entity.Name;
            tempUpdate.Surname = entity.Surname;
            tempUpdate.DateOfBirth = entity.DateOfBirth;
            tempUpdate.Gender = (int)entity.Gender;
            tempUpdate.Address = entity.Address;
            tempUpdate.Email = entity.Email;
            adapter.Update(tempUpdate);
        }
    }
}
