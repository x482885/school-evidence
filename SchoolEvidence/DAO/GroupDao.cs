﻿using SchoolEvidence.Entities;
using SchoolEvidence.Entities.Structs;
using System;
using System.Collections.Generic;
using System.Linq;
using static SchoolEvidence.DatabaseDataSet;

namespace SchoolEvidence.DAO
{
    public class GroupDao : DataAcessObject<Group, GroupSetRow, GroupStruct>
    {
        public DatabaseDataSetTableAdapters.GroupSetTableAdapter adapter { get; private set; } = new DatabaseDataSetTableAdapters.GroupSetTableAdapter();
        public void delete(Group entity)
        {
            if (!adapter.GetData().Any(row => row.Id == entity.Id)) return;
            GroupSetRow tempDelete = adapter.GetData().FindById(entity.Id);
            adapter.Delete(
                tempDelete.Id,
                tempDelete.Day,
                tempDelete.Time,
                tempDelete.SubjectId
                );
        }

        public ICollection<Group> findAll()
        {
            return adapter.GetData().Select(row => toEntity(row)).ToList();
        }

        public void insert(GroupStruct strct)
        {
            adapter.Insert(
                strct.Name,
                (int)strct.Day,
                strct.Time,
                strct.SubjectId
                );
        }

        public Group toEntity(GroupSetRow row)
        {
            return new Group(
                row.Id,
                row.Name,
                (DayOfWeek)row.Day,
                row.Time,
                row.SubjectId
                );
        }

        public void update(Group entity)
        {
            if (!adapter.GetData().Any(row => row.Id == entity.Id)) return;
            GroupSetRow tempUpdate = adapter.GetData().FindById(entity.Id);
            tempUpdate.Name = entity.Name;
            tempUpdate.Day = (int)entity.Day;
            tempUpdate.Time = entity.Time;
            tempUpdate.SubjectId = entity.SubjectId;
            adapter.Update(tempUpdate);
        }
    }
}
