﻿using SchoolEvidence.Entities;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using static SchoolEvidence.DatabaseDataSet;

namespace SchoolEvidence.DAO
{
    public class ExcuseDao : DataAcessObject<Excuse, ExcuseSetRow, ExcuseStruct>
    {
        public DatabaseDataSetTableAdapters.ExcuseSetTableAdapter adapter { get; private set; } = new DatabaseDataSetTableAdapters.ExcuseSetTableAdapter();

        public void delete(Excuse entity)
        {
            if (!adapter.GetData().Any(row => row.Id == entity.Id)) return;
            ExcuseSetRow tempDelete = adapter.GetData().FindById(entity.Id);
            adapter.Delete(
                tempDelete.Id,
                tempDelete.StudentId,
                tempDelete.DateFrom,
                tempDelete.DateTo
                );
        }

        public ICollection<Excuse> findAll()
        {
            return adapter.GetData().Select(row => toEntity(row)).ToList();
        }

        public void insert(ExcuseStruct strct)
        {
            adapter.Insert(
                strct.StudentID,
                strct.DateFrom,
                strct.DateTo,
                strct.Reason
                );
        }

        public Excuse toEntity(ExcuseSetRow row)
        {
            return new Excuse(
                row.Id,
                row.StudentId,
                row.DateFrom,
                row.DateTo,
                row.Reason
                );
        }

        public void update(Excuse entity)
        {
            if (!adapter.GetData().Any(row => row.StudentId == entity.Id)) return;
            ExcuseSetRow tempUpdate = adapter.GetData().FindById(entity.Id);
            tempUpdate.StudentId = entity.StudentID;
            tempUpdate.DateFrom = entity.DateFrom;
            tempUpdate.DateTo = entity.DateTo;
            tempUpdate.Reason = entity.Reason;
            adapter.Update(tempUpdate);
        }
    }
}
