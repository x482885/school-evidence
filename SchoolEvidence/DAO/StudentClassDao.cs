﻿using SchoolEvidence.Entities;
using System.Collections.Generic;
using System.Linq;
using static SchoolEvidence.DatabaseDataSet;

namespace SchoolEvidence.DAO
{
    public class StudentClassDao : DataAcessObject<StudentClass, StudentClassSetRow, StudentClass>
    {
        public DatabaseDataSetTableAdapters.StudentClassSetTableAdapter adapter { get; private set; } = new DatabaseDataSetTableAdapters.StudentClassSetTableAdapter();
        public void delete(StudentClass entity)
        {
            if (!adapter.GetData().Any(row => row.StudentId == entity.StudentId && row.ClassId == entity.ClassId)) return;
            StudentClassSetRow tempDelete = adapter.GetData().FindByStudentIdClassId(entity.StudentId, entity.ClassId);
            adapter.Delete(
                tempDelete.StudentId,
                tempDelete.ClassId,
                tempDelete.IsExcuseIdNull() ? (int?)null : tempDelete.ExcuseId,
                tempDelete.Attended
                );
        }

        public ICollection<StudentClass> findAll()
        {
            return adapter.GetData().Select(row => toEntity(row)).ToList();
        }

        public void insert(StudentClass entity)
        {
            if (findAll().Any(e => e.StudentId == entity.StudentId && e.ClassId == entity.ClassId)) return;
            adapter.Insert(
                entity.StudentId,
                entity.ClassId,
                entity.ExcuseId,
                entity.Attended
                );
        }

        public StudentClass toEntity(StudentClassSetRow row)
        {
            return new StudentClass(
                row.StudentId,
                row.ClassId,
                row.Attended,
                row.IsExcuseIdNull() ? (int?)null : row.ExcuseId
                );
        }

        public void update(StudentClass entity)
        {
            if (!adapter.GetData().Any(row => row.StudentId == entity.StudentId && row.ClassId == entity.ClassId)) return;
            StudentClassSetRow tempUpdate = adapter.GetData().FindByStudentIdClassId(entity.StudentId, entity.ClassId);
            if (entity.ExcuseId == null)
            {
                tempUpdate.SetExcuseIdNull();
            }
            else
            {
                tempUpdate.ExcuseId = (int)entity.ExcuseId;
            }
            tempUpdate.Attended = entity.Attended;
            adapter.Update(tempUpdate);
        }
    }
}
