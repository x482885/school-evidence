﻿using SchoolEvidence.Entities;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using static SchoolEvidence.DatabaseDataSet;

namespace SchoolEvidence.DAO
{
    public class ClassDao : DataAcessObject<Class, ClassSetRow, ClassStruct>
    {
        public DatabaseDataSetTableAdapters.ClassSetTableAdapter adapter { get; private set; } = new DatabaseDataSetTableAdapters.ClassSetTableAdapter();

        public void delete(Class entity)
        {
            if (!adapter.GetData().Any(row => row.Id == entity.Id)) return;
            ClassSetRow tempDelete = adapter.GetData().FindById(entity.Id);
            adapter.Delete(
                tempDelete.Id,
                tempDelete.DateTime,
                tempDelete.GroupId
                );
        }

        public ICollection<Class> findAll()
        {
            return adapter.GetData().Select(row => toEntity(row)).ToList();
        }

        public void insert(ClassStruct strct)
        {
            adapter.Insert(
                strct.DateTime,
                strct.GroupId
                );
        }

        public Class toEntity(ClassSetRow row)
        {
            return new Class(
                row.Id,
                row.DateTime,
                row.GroupId
                );
        }

        public void update(Class entity)
        {
            if (!adapter.GetData().Any(row => row.Id == entity.Id)) return;
            ClassSetRow tempUpdate = adapter.GetData().FindById(entity.Id);
            tempUpdate.DateTime = entity.DateTime;
            tempUpdate.GroupId = entity.GroupId;
            adapter.Update(tempUpdate);
        }
    }
}
