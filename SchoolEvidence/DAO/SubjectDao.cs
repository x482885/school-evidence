﻿using SchoolEvidence.Entities;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using static SchoolEvidence.DatabaseDataSet;

namespace SchoolEvidence.DAO
{
    public class SubjectDao : DataAcessObject<Subject, SubjectSetRow, SubjectStruct>
    {
        public DatabaseDataSetTableAdapters.SubjectSetTableAdapter adapter { get; private set; } = new DatabaseDataSetTableAdapters.SubjectSetTableAdapter();

        public void delete(Subject entity)
        {
            if (!adapter.GetData().Any(row => row.Id == entity.Id)) return;
            SubjectSetRow tempDelete = adapter.GetData().FindById(entity.Id);
            adapter.Delete(
                tempDelete.Id,
                tempDelete.Year,
                tempDelete.Term,
                tempDelete.Credit,
                tempDelete.DurationHours,
                tempDelete.Completion
                );
        }

        public ICollection<Subject> findAll()
        {
            return adapter.GetData().Select(row => toEntity(row)).ToList();
        }

        public void insert(SubjectStruct strct)
        {
            adapter.Insert(
                strct.Code,
                strct.Name,
                strct.Year,
                (int)strct.Term,
                strct.Credit,
                strct.DurationHours,
                (int)strct.CompletionType,
                strct.Prerequisites
                );
        }

        public Subject toEntity(SubjectSetRow row)
        {
            return new Subject(
                row.Id,
                row.Name,
                row.Code,
                row.Year,
                (Term)row.Term,
                row.Credit,
                row.DurationHours,
                (CompletionType)row.Completion,
                row.Prerequisites
                );
        }

        public void update(Subject entity)
        {
            if (!adapter.GetData().Any(row => row.Id == entity.Id)) return;
            SubjectSetRow tempUpdate = adapter.GetData().FindById(entity.Id);
            tempUpdate.Name = entity.Name;
            tempUpdate.Code = entity.Code;
            tempUpdate.Year = entity.Year;
            tempUpdate.Term = (int)entity.Term;
            tempUpdate.Credit = entity.Credit;
            tempUpdate.DurationHours = entity.DurationHours;
            tempUpdate.Completion = (int)entity.CompletionType;
            tempUpdate.Prerequisites = entity.Prerequisites;
            adapter.Update(tempUpdate);
        }
    }
}
