﻿using SchoolEvidence.Entities;
using System.Collections.Generic;
using System.Linq;
using static SchoolEvidence.DatabaseDataSet;

namespace SchoolEvidence.DAO
{
    public class GroupStudentDao : DataAcessObject<GroupStudent, GroupStudentSetRow, GroupStudent>
    {
        public DatabaseDataSetTableAdapters.GroupStudentSetTableAdapter adapter { get; private set; } = new DatabaseDataSetTableAdapters.GroupStudentSetTableAdapter();
        public void delete(GroupStudent entity)
        {
            if (!adapter.GetData().Any(row => row.StudentId == entity.StudentId && row.GroupId == entity.GroupId)) return;
            GroupStudentSetRow tempDelete = adapter.GetData().FindByStudentIdGroupId(entity.StudentId, entity.GroupId);
            adapter.Delete(
                tempDelete.StudentId,
                tempDelete.GroupId
                );
        }

        public ICollection<GroupStudent> findAll()
        {
            return adapter.GetData().Select(row => toEntity(row)).ToList();
        }

        public void insert(GroupStudent entity)
        {
            if (findAll().Any(gs => gs.StudentId == entity.StudentId && gs.GroupId == entity.GroupId)) return;
            adapter.Insert(
                entity.StudentId,
                entity.GroupId
                );
        }

        public GroupStudent toEntity(GroupStudentSetRow row)
        {
            return new GroupStudent(
                row.StudentId,
                row.GroupId
                );
        }

        public void update(GroupStudent entity)
        {
            return;
        }
    }
}
