﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace SchoolEvidence.Tools
{
    public class ExcuseIdToIsExcusedBoolean : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!(value is int?)) return "No";
            return ((int?)value).HasValue ? string.Format("Yes - {0}", (int?)value) : "No";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null) return null;
            string[] parts = value.ToString().Split('-');
            return parts.Length > 1 ? parts[1] : null;
        }
    }
}
