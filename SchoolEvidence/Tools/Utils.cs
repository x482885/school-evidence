﻿using System.Text.RegularExpressions;
using System.Windows.Input;
using System.Windows.Media;

namespace SchoolEvidence.Tools
{
    public class Utils
    {
        public static Brush warningColor = (Brush)new BrushConverter().ConvertFrom("#FFFF8C8E");
        public static void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        public static bool IsNullOrEmptyOrBlank(string sequence)
        {
            return string.IsNullOrEmpty(sequence) || string.IsNullOrWhiteSpace(sequence);
        }
    }
}
